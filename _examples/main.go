package main

import (
	"fmt"
	"time"

	"gitlab.com/diamondburned/clocker"
)

func main() {
	var (
		i int
		c = clocker.NewTicker(time.Second)
	)

	for t := range c.C {
		fmt.Printf("Tick %d: %s\n", i, t.Format(time.ANSIC))
		i++

		if i == 5 {
			c.Stop()
		}
	}
}
