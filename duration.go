package clocker

import "time"

func getDurationForNextFrame(frame time.Duration) time.Duration {
	tick := time.Now().Round(frame)
	if s := tick.Sub(time.Now()); s > 0 {
		return s
	}

	return tick.Add(frame).Sub(time.Now())
}
