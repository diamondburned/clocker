# clocker

A Ticker library that uses the clock to determine when to tick, instead of using the time the ticker was made.

### Accuracy

According to the test: 1ms
