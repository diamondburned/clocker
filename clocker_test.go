package clocker

import (
	"testing"
	"time"
)

func TestTicker(t *testing.T) {
	d := time.Second * 15

	ticker := NewTicker(d)
	println("New ticker initialized")

	tick := <-ticker.C
	println("Tick received")

	now := time.Now().Round(d)

	ticker.Stop()
	println("Tick stopped")

	// Accuracy: time.Millisecond
	if tick.Round(time.Millisecond) != now {
		t.Fatalf(
			"Time is not equal: %s, %s",
			tick.Format(time.RFC3339Nano),
			now.Format(time.RFC3339Nano),
		)
	}
}
